import os
import sys
from time import sleep
from playwright.sync_api import Page
import pytest
from os.path import dirname, abspath
from tools.get_log import GetLog
from tools.read_json import get_data

logincommon_path = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
sys.path.insert(0, logincommon_path)
logger = GetLog.get_log()


def test_baidu(page: Page):

    """
    操作步骤：
    1）打开http://www.baidu.com
    2)输入“playwright”，点击百度一下按钮
    期望结果：
    页面标题显示为“playwright_百度搜索”
    """

    casename = os.path.splitext(os.path.basename(__file__))[0]
    logger.info("正在执行用例:{}".format(casename))

    # Go to https://www.baidu.com/
    page.goto("https://www.baidu.com/")

    # Click input[name="wd"]
    page.click("input[name=\"wd\"]")

    # Fill input[name="wd"]
    page.fill("input[name=\"wd\"]", "playwright")

    # Click text=百度一下
    with page.expect_navigation():
        page.click("text=百度一下")

    sleep(2)

    # 检查页面标题是否显示为“playwright_百度搜索”，如果不是，则抛出异常
    try:
        assert "playwright_百度搜索" == page.title()
        logger.info("用例{}的断言结果为{}".format(casename, "playwright_百度搜索" == page.title()))
    except Exception as e:
        logger.error("用例{}的断言结果为{},错误信息是：{}".format(casename, "playwright_百度搜索" == page.title(), e))
        raise

    # Close page
    page.close()
