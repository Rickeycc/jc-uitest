# author: 宿爽
# date: 2021/11/13
# email: sushuang@jiachengnet.com


import logging.handlers
import os
from config import PRO_PATH


class GetLog:
    __logger = None

    # 获取日志器
    @classmethod
    def get_log(cls):
        if cls.__logger is None:
            cls.__logger = logging.getLogger()
            cls.__logger.setLevel(logging.INFO)
            filename = PRO_PATH + os.sep + "logs" + os.sep + "uc.log"
            fh = logging.handlers.TimedRotatingFileHandler(filename,
                                                           when='midnight', interval=1, backupCount=3, encoding="utf-8")
            """
            when='midnight':一天一夜
            interval=1：每隔一个when生成一个日志文件
            backupCount=7：保存7天的日志，备份数量

            """
            fmt = logging.Formatter(
                "%(asctime)s --%(name)s-- %(filename)s.%(funcName)s():line %(lineno)d [%(levelname)s]:%(message)s")
            fh.setFormatter(fmt)
            cls.__logger.addHandler(fh)
        return cls.__logger

