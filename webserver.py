import sys
import http.server
import socketserver
import os


class webserver():
    PORT = 8000
    Handler = http.server.SimpleHTTPRequestHandler
    httpd = socketserver.TCPServer(("", PORT), Handler)

    def __init__(self):
        web_dir = os.path.join(os.path.dirname(__file__), 'report')
        os.chdir(web_dir)

    def start(self):
        print("serving at port", self.PORT)
        try:
            self.httpd.serve_forever()
        except KeyboardInterrupt as e:
            print("exit")

    def stop(self):
        print("stop web server")
        self.httpd.server_close()

    def __del__(self):
        self.stop()


if __name__ == "__main__":
    web = webserver()
    web.start()
